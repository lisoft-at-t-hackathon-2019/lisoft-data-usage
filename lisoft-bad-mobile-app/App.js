import * as React from 'react';
import { Text, View, StyleSheet, Button, Alert, TextInput } from 'react-native';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      view: true,
      amouth: 0,
      type: 0,
      name: ""
    }
  }

  render() {
    if(this.state.view == true) {
      return (
        <>
          <View style={styles.container}>
            <Text style={styles.paragraph}>
              ADD FREEZER
            </Text>
            <Card>
              <Text style={styles.loweparagraph}>
                Name
              </Text>
              <TextInput style={{ height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', margin: '10px' }} keyboardType="number" onChangeText={(text) => {
                this.setState({name: text});
              }}/>
              <Button style={{ padding: "16px" }} title="Submit" onPress={() => {
                alert(this.state.name);
              }}/>
            </Card>
          </View>
          <View>
            <Button title="FREEZER"
              onPress={() => this.setState({view: true})}
            />
            <Button title="PRODUCT"
              onPress={() => this.setState({view: false})}
            />
          </View>
        </>
      );
    } else {
      return (
        <>
          <View style={styles.container}>
            <Text style={styles.paragraph}>
              ADD PRODUCT
            </Text>
            <Card>
              <Text style={styles.loweparagraph}>
                Amouth
              </Text>
              <TextInput style={{ height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', margin: '10px' }} keyboardType="number" onChangeText={(text) => {
                this.setState({amouth: text});
              }}/>
              <Text style={styles.loweparagraph}>
                Type
              </Text>
              <TextInput style={{ height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', margin: '10px' }} onChangeText={(text) => {
                this.setState({type: text});
              }}/>
              <Button style={{ padding: "16px" }} title="Submit" onPress={() => {
                alert(this.state.amouth + " " + this.state.type);
              }}/>
            </Card>
          </View>
          <View>
            <Button title="FREEZER"
              onPress={() => this.setState({view: true})}
            />
            <Button title="PRODUCT"
              onPress={() => this.setState({view: false})}
            />
          </View>
        </>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: '32px',
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loweparagraph: {
    margin: 8,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  }
});
