function toSeconds(dateString){                 //From classic to Unix Date format
    var date = new Date(dateString);
    var seconds = date.getTime() / 1000;
    return seconds;
}

function toDateTime(secs) {                     //From Unix Date format to classic
  var t = new Date(1970, 0, 1, 1); 
  t.setSeconds(secs);
  return t;
}

function logOut(){
    Cookies.remove('eid');
}

$( "#logout" ).click(function() {
    logOut();
    location.reload();
})

function firstSetUp(){              //Set up
    var html_vyskakovaci_okno = "<div id='vyskakovaci_okno' class='vyskakovaci-okno'><div class='inner'><h2>Sign up your device!</h2><input id='geteidID' type='text' name='eid'><a class='button' id='submit'>Submit</a></div></div>"
    $("body").append(html_vyskakovaci_okno)
    $( "#submit" ).click(function() {
        eid = $("#geteidID").val();
        if(eid.length != 21){alert("Fuck"); return;}
        console.log(eid)
        Cookies.set('eid', eid);
        $("#vyskakovaci_okno").remove()
        main();
    });
}

function insertActualTemp() {          //Insert actual template
    $.getJSON("https://newagent-a153e.firebaseio.com/users/1/" + eid + "/actualTemp.json", callbackBleeh)
    function callbackBleeh(data)
    {
        $('#aktualni_teplota').append(data);
    }
}
function insertEid() {              //Insert eid
    $('#eid').append(eid);
}

function insertName() {             //Insert name from DB
    $.getJSON("https://newagent-a153e.firebaseio.com/users/1/" + eid + "/name.json", callbackBleeh)
    function callbackBleeh(data)
    {
        $('#name').append(data);
    }
}
var j = 1;

function insertNewProduct(data,id,name,options,pieces,minimum) {                    //Function for insert a new product
    console.log(pieces)
    var warn = ""
    if(pieces<=minimum) { warn = "warn" }
    var html_produktu = "<div class='product' id='#product" + id + "'><div class='row'><div class='col-md-4 details'><div class='row'><div class='col-md-12'><h3 class='name_of_product'>" + name + "</h3><p>ID:&nbsp;<span class='produkt_id'>" + id + "</span></p></div></div></div><div class='col-md-5 pieces'><div class='row'><p><span class='zbyvajici_kusy " + warn + "'>" + pieces + "</span>&nbsp;ks</p></div></div><div class='col-md-3 graph'><div class='row'><canvas id='product_aviability_ID" + j + "'></canvas></div></div></div></div>"
    
    $('#products').append(html_produktu);
    
    var myChart = new Chart(document.getElementById("product_aviability_ID"+(j)).getContext("2d"), {
      type: 'line',
      data: {
        datasets: [{
          label: 'Počet produktů',
          data: data,
          backgroundColor: [
            'rgba(255, 99, 132, 0)'
          ],
          borderColor: [
            'rgba(255,99,132,1)'
          ],
          borderWidth: 1
        }]
      },
        options: options
    });
    
    j++;
}


function insertAllProducts() {                                                                                  //Function for insert all products from DB
    $.getJSON("https://newagent-a153e.firebaseio.com/users/1/" + eid + "/products.json", callbackBleeh)
    function callbackBleeh(data)
    {
        var i,j;
        var data_of_product,time,amount;
        var dataG = [];
        for (i = 1; i < data.length; i++) { 
            data_of_product = data[i].historicalData;
            //console.log(data_of_product)
            for (j in data_of_product) { 
                time = toDateTime(j);
                amount = data_of_product[j]
                dataG.push({
                  x: new Date(time),
                  y: amount
                });
            }
            console.log(data[i])
            insertNewProduct(dataG,i,data[i].name,optionsProduct,data[i].amount,data[i].minimal)
            var dataG = [];
        } 
        console.log(dataG)
    }
}


function insertTempTableFromDB(){                                                                               //Function for fetch from DB and insert to table of temperature
    $.getJSON("https://newagent-a153e.firebaseio.com/users/1/" + eid + "/historicalData.json", callbackBleeh)
    function callbackBleeh(data)
    {
        var i=0,j;
        var data_of_product,time,amount;
        var dataG = [];
            console.log(1);
        
            for (j in data) { 
                console.log(i);
                time = toDateTime(j);
                amount = data[j]
                dataG.push({
                  x: new Date(time),
                  y: amount
                });
                i++;
            }
        insertTempTable(dataG,optionsTemp)
        
        
        console.log(dataG)
    }
}

function insertTempTable(data,options){                                                         //Function for insert table of temperature
    var myChart = new Chart(document.getElementById("examChart").getContext("2d"), {
      type: 'line',
      data: {
        datasets: [{
          label: 'Teplota',
          data: data,
          backgroundColor: [
            'rgba(255, 99, 132, 0)'
          ],
          borderColor: [
            'rgba(255,99,132,1)'
          ],
          borderWidth: 1
        }]
      },
        options: options
    });
}

var optionsTemp = {
        responsive: true,
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    unit: 'month'
                }
            }]
        },
        legend: {
            display: false,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        },
        gridLines: {
            lineWidth: 1
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        }
    }


var optionsProduct = {
        responsive: true,
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    unit: 'month'
                }
            }]
        },
        legend: {
            display: false,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        },
        gridLines: {
            lineWidth: 1,
            display: false
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        ticks: {
            display: false
        },
        major: {
            enabled: false
        },
         legend: {
            display: false
         },
         tooltips: {
            enabled: false
         }
    }


if(!Cookies.get('eid')){                        //If you are new, we need to set up
    firstSetUp()
} else {
    var eid = Cookies.get('eid');
    main();
}

function main(){
    insertEid();
    insertName();
    insertActualTemp();
    insertAllProducts();
    insertTempTableFromDB();
    //insertNewProduct(dataProduct,1,"Burger",optionsProduct,600,20);
    //insertNewProduct(dataProduct,2,"Maso",optionsProduct,10,20);
    //insertNewProduct(dataProduct,3,"Salát",optionsProduct,300,20);
    //insertNewProduct(dataProduct,4,"Omáčky",optionsProduct,1000,20);
    //insertTempTable(dataTemp,optionsTemp);
}